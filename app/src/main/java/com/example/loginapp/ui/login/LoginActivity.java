package com.example.loginapp.ui.login;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.loginapp.R;
import com.example.loginapp.bd.MyOpenHelper;

public class LoginActivity extends AppCompatActivity {

    private MyOpenHelper db;
    private Cursor fila;
    private ProgressDialog dialog;
    Conexion con;
    String deviceID = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        deviceID = new deviceID().id(this);


        try {
            db = new MyOpenHelper(this);
        } catch (Exception ex) {
            Mensaje(ex.getMessage());
        }


    }


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Desea Salir?????")
                .setCancelable(false)
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


    public void ingresoBtn(View view) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        EditText email = findViewById(R.id.username);
        EditText pass = findViewById(R.id.password);
        String text = "";

        dialog.setMessage("Validando Datos!!!!");

        if (email.getText().toString().isEmpty()) {
            text = "Debe ingresar un Nombre de Usuario válido";
        } else if (pass.getText().toString().isEmpty()) {
            text = "Debe ingresar una Contraseña";
        } else {

            MyOpenHelper user = new MyOpenHelper(this);
            String usuario = email.getText().toString().trim();
            String password = MD5.getMd5Hash(pass.getText().toString());

            fila = new Usuarios().verficaUsuario(usuario, password, user);

            new loginPass().execute();

        }


        if (text != "") {
            Mensaje(text);
        }


    }

    public void registroBtn(View view) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = new Intent(LoginActivity.this, Register.class);

        startActivity(intent);
    }


    public class loginPass extends AsyncTask<Void, Void, String> {
        public void onPreExecute() {
            dialog.show();
        }

        public String doInBackground(Void... unused) {
            String result = "";

            try {
                if (!fila.moveToFirst()) {
                    result = null;
                } else {
                    Intent intent = new Intent(LoginActivity.this, Main.class);
                    intent.putExtra("NombreApe", fila.getString(1) + " " + fila.getString(2));

                    fila.close();
                    db.close();

                    startActivity(intent);
                }
            } catch (Exception e) {
                result = null;
            }

            return result;
        }


        public void onPostExecute(String result) {

            if (result == null) {
                Mensaje("Usuario Incorrecto!!!!!");
            }

            dialog.dismiss();
        }
    }


    public Void Mensaje(String texto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(texto)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        return null;
    }


    //VERIFICA CONEXION A INTERNET

    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            onNetworkChange(ni);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        unregisterReceiver(networkStateReceiver);
        super.onPause();
    }

    private void onNetworkChange(NetworkInfo networkInfo) {
        if (networkInfo != null) {
            if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                Log.d("MenuActivity", "CONNECTED");
                dialog.setMessage("Sincronizando Tablas!!!!");
                new sincronizaTablas().execute();
            } else {
                Log.d("MenuActivity", "DISCONNECTED");

             String a =    "pruebaaaa";
            }
        }
    }


    //FIN VERIFICA CONEXION A INTERNET


    public class sincronizaTablas extends AsyncTask<Void, Void, Integer> {

        public void onPreExecute() {
            dialog.show();
        }

        public Integer doInBackground(Void... unused) {
            int result = -1;


            try {
               /* Usuarios syncTablaUsuarios;
                ResultSet tablaUsuariosSQL;
                syncTablaUsuarios = new Usuarios();
                tablaUsuariosSQL = syncTablaUsuarios.SincronizaTablaUsuarioSQL();

                if (tablaUsuariosSQL.isBeforeFirst()) {
                    result = db.insertaDatosTablaUsuariosSQL(tablaUsuariosSQL) < 0 ? null : "";
                } else {
                    result = null;
                }
                */

                result = db.insertaDatosTablaUsuariosSQL(deviceID);



            } catch (Exception e) {
                result = -1;
            }
            return result;

        }


        public void onPostExecute(Integer result) {
            Button registro = findViewById(R.id.register);

            if (result == -1)
            {
                Mensaje("Error al Sincronizar Tablas!!!");
            }
            else if(result == -2)
            {
                Mensaje("Debes Registrar un Usuario!!!");
            }
            else if(result == -3)
            {
                registro.setEnabled(false);
                //Mensaje("Usuario existente en BD SQLSERVER!!!");
            }
            else {
                Mensaje("Sincronización Correcta!!!");
                registro.setEnabled(false);
            }

            db.close();

            dialog.dismiss();
        }
    }

}


